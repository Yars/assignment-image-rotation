//
// Created by yaros on 02.02.2022.
//

#ifndef IMAGE_ROTATE_IMAGE_H
#define IMAGE_ROTATE_IMAGE_H

#include <stdint.h>

typedef struct {
    uint8_t b, g, r;
} pixel;

typedef struct {
    uint64_t width, height;
    pixel *data;
} image;

enum image_status {
    IMAGE_INITIALIZED = 0,
    IMAGE_INITIALIZATION_FAILED
};

enum image_status image_init(image *img, uint64_t width, uint64_t height);

void image_destroy(image *img);

#endif //IMAGE_ROTATE_IMAGE_H
