#ifndef IMAGE_ROTATE_TRANSFORMATIONS_H
#define IMAGE_ROTATE_TRANSFORMATIONS_H

#include <stdlib.h>
#include <string.h>
#include "image.h"

enum transformation_status {
    TRANSFORMATION_OK = 0,
    TRANSFORMATION_ERROR
};

enum transformation_status invert(const image *source, image *dest);
enum transformation_status rotate(const image *source,  image *dest);

#endif //IMAGE_ROTATE_TRANSFORMATIONS_H
