#ifndef ASSIGNMENT_IMAGE_ROTATION_UTILS_H
#define ASSIGNMENT_IMAGE_ROTATION_UTILS_H

#include "bmp.h"
#include "transformations.h"

void explain_read_error(enum read_status status);
void explain_write_error(enum write_status status);
void explain_transformation_error(enum transformation_status status);

#endif //ASSIGNMENT_IMAGE_ROTATION_UTILS_H
