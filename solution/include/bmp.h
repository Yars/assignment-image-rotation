//
// Created by yaros on 02.02.2022.
//

#ifndef IMAGE_ROTATE_BMP_H
#define IMAGE_ROTATE_BMP_H

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "image.h"

#ifdef _WIN64
#pragma pack(push, 1)
typedef struct {
    // BMP file header
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    // BMP file info
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
} bmp_header;
#pragma pack(pop)
#else
typedef struct __attribute__((packed)) {
    // BMP file header
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    // BMP file info
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
} bmp_header;
#endif

/*  deserializer   */
enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_BAD_FILE,
    READ_INVALID_PARAMETER,
    READ_IMAGE_INITIALIZATION_FAILED
};

#define BMP_SIGNATURE 0x4d42
#define BMP_BITS 24
#define BMP_PLANES 1
#define BMP_PPM 2834

enum read_status read_header(FILE *in, bmp_header *header);

enum read_status from_bmp(FILE *in,  image *img);

/*  serializer   */
enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR
};

enum write_status to_bmp(FILE *out, image const *img);

#endif //IMAGE_ROTATE_BMP_H
