//
// Created by yaros on 02.02.2022.
//
#include "bmp.h"

enum read_status read_header(FILE *in, bmp_header *header) {
    if (header == NULL) {
        return READ_INVALID_PARAMETER;
    }
    if (in == NULL) {
        return READ_BAD_FILE;
    } else {
        size_t n = fread(header, sizeof(bmp_header), 1, in);
        if (n != 1) {
            return READ_INVALID_HEADER;
        } else {
            if (header->bfType != BMP_SIGNATURE) {
                return READ_INVALID_SIGNATURE;
            }
            if (header->biBitCount != BMP_BITS) {
                return READ_INVALID_BITS;
            }
            if (fseek(in, 0L, SEEK_SET) == 0) {
                return READ_OK;
            } else {
                return READ_BAD_FILE;
            }
        }
    }
}

enum read_status from_bmp(FILE *in, image *img) {
    bmp_header header;
    if (in == NULL) {
        return READ_BAD_FILE;
    } else {
        enum read_status status = read_header(in, &header);
        if (status != READ_OK) {
            return status;
        }

        if (img == NULL) {
            return READ_INVALID_PARAMETER;
        }

        enum image_status i_status = image_init(img, header.biWidth, header.biHeight);
        if (i_status == IMAGE_INITIALIZATION_FAILED) {
            image_destroy(img);
            return READ_IMAGE_INITIALIZATION_FAILED;
        } else {
            // Calculate garbage bytes number
            unsigned long garbage_bytes = (4 - (header.biWidth * sizeof(pixel)) % 4) % 4;

            // Move to the bitmap start
            if (fseek(in, header.bOffBits, SEEK_SET) != 0) {
                return READ_BAD_FILE;
            } else {
                // Loop over bitmap array
                size_t n = 0;

                for (size_t i = 0; i < header.biHeight; i++) {
                    n = fread(&img->data[i * header.biWidth], sizeof(pixel), header.biWidth, in);
                    if (n != header.biWidth) {
                        break;
                    }
                    if (garbage_bytes) {
                        if (fseek(in, (long) garbage_bytes, SEEK_CUR) != 0) {
                            return READ_BAD_FILE;
                        }
                    }
                }
                return READ_OK;
            }
        }
    }
}

enum write_status to_bmp(FILE *out, image const *img) {
    if (out == NULL)
        return WRITE_ERROR;

    bmp_header header;
    header.bfType = BMP_SIGNATURE;
    header.bfReserved = 0;
    header.bOffBits = sizeof(bmp_header);
    header.biSize = 40;
    header.biWidth = img->width;
    header.biHeight = img->height;
    header.biPlanes = BMP_PLANES;
    header.biBitCount = BMP_BITS;
    header.biCompression = 0;
    header.biXPelsPerMeter = BMP_PPM;
    header.biYPelsPerMeter = BMP_PPM;
    header.biClrUsed = 0;
    header.biClrImportant = 0;

    char garbage_buffer[] = {0, 0, 0};
    unsigned long garbage_bytes = (4 - (img->width * sizeof(pixel)) % 4) % 4;

    unsigned long file_size = img->width * img->height * sizeof(pixel) + header.bOffBits;
    header.bfileSize = file_size;
    header.biSizeImage = img->width * img->height * sizeof(pixel);

    if (fwrite(&header, sizeof(bmp_header), 1, out) != 1) {
        return WRITE_ERROR;
    }
    size_t n;
    for (size_t y = 0; y < img->height; y++) {
        n = fwrite(&img->data[y * header.biWidth], sizeof(pixel), header.biWidth, out);
        if (n != header.biWidth) {
            break;
        }
        if (garbage_bytes) {
            if (fwrite(garbage_buffer, garbage_bytes, 1, out) != 1) {
                return WRITE_ERROR;
            }
        }
    }

    return WRITE_OK;
}
