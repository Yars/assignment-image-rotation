#include "bmp.h"
#include "transformations.h"
#include "utils.h"

int main(int argc, char **argv) {
    if (argc < 2) {
        printf("Invalid arguments\nUSAGE:\n  ./image-transformer <source-image> <transformed-image>\n");
        return 0;
    }
    image img;
    bmp_header header;
    FILE *bmp_file = NULL;
    enum read_status r_status;

    bmp_file = fopen(argv[1], "rb");
    r_status = read_header(bmp_file, &header);
    if (r_status != READ_OK) {
        image_destroy(&img);
        explain_read_error(r_status);
        return r_status;
    }
    r_status = from_bmp(bmp_file, &img);
    fclose(bmp_file);
    if (r_status != READ_OK) {
        image_destroy(&img);
        explain_read_error(r_status);
        return r_status;
    }
    image rotated;
    enum transformation_status t_status = rotate(&img, &rotated);
    if (t_status != TRANSFORMATION_OK) {
        image_destroy(&img);
        image_destroy(&rotated);
        explain_transformation_error(t_status);
        return TRANSFORMATION_ERROR;
    }
    bmp_file = fopen(argv[2], "wb");
    enum write_status w_status = to_bmp(bmp_file, &rotated);
    fclose(bmp_file);
    if (w_status != WRITE_OK) {
        image_destroy(&img);
        image_destroy(&rotated);
        explain_write_error(w_status);
        return WRITE_ERROR;
    }
    image_destroy(&rotated);
    image_destroy(&img);
    return 0;
}
