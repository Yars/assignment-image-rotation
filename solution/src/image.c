#include "image.h"
#include <stdlib.h>

enum image_status image_init(image *img, uint64_t width, uint64_t height) {
    if (!img) {
        return IMAGE_INITIALIZATION_FAILED;
    } else {
        img->width = width;
        img->height = height;
        img->data = (pixel *) malloc(sizeof(pixel) * width * height);
        if (img->data == NULL) {
            return IMAGE_INITIALIZATION_FAILED;
        } else {
            return IMAGE_INITIALIZED;
        }
    }
}

void image_destroy(image *img) {
    if (img != NULL) {
        if (img->data != NULL) {
            free(img->data);
        }
    }
}
