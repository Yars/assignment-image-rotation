#include "utils.h"

const char *image_status_text[] = {
        "Image bitmap initialized",
        "Image bitmap initialization error"
};

const char *read_status_text[] = {
        "BMP file read OK",
        "Invalid BMP signature (not 'BM')",
        "Invalid color depth. Only 24-bit files supported",
        "Invalid BMP header",
        "Broken BMP file (or not BMP)",
        "Invalid parameter passed to the function",
        "Image bitmap initialization filed"
};

const char *write_status_text[] = {
    "BMP file write OK",
    "BMP file write error"
};

const char *transformation_status_text[] = {
        "Image transformation OK",
        "Image transformation error"
};

void explain_image_error(enum image_status status) {
    printf("Image initialization status: %s", image_status_text[status]);
}

void explain_read_error(enum read_status status) {
    printf("BMP file read status: %s", read_status_text[status]);
}

void explain_write_error(enum write_status status) {
    printf("BMP file write status: %s", write_status_text[status]);
}

void explain_transformation_error(enum transformation_status status) {
    printf("Image transformation status: %s", transformation_status_text[status]);
}
