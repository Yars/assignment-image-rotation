#include "transformations.h"
#include "utils.h"

enum transformation_status invert(const image *source, image *dest) {
    if (source == NULL) {
        return TRANSFORMATION_ERROR;
    }

    image_init(dest, source->width, source->height);
    for (int y = 0; y < source->height; y++) {
        for (int x = 0; x < source->width; x++) {
            pixel p = source->data[source->width * y + x];
            p.r = 255 - p.r;
            p.g = 255 - p.g;
            p.b = 255 - p.b;
            dest->data[source->width * y + x] = p;
        }
    }

    return TRANSFORMATION_OK;
}

enum transformation_status rotate(const image *source, image *dest) {
    if (source == NULL) {
        return TRANSFORMATION_ERROR;
    }

    if (dest == NULL) {
        return TRANSFORMATION_ERROR;
    } else {
        enum image_status i_status = image_init(dest, source->height, source->width);

        if (i_status == IMAGE_INITIALIZATION_FAILED) {
            image_destroy(dest);
            //image_explain_status(i_status);
            return TRANSFORMATION_ERROR;
        } else {

            for (int y = 0; y < source->height; y++) {
                for (int x = 0; x < source->width; x++) {
                    pixel p = source->data[source->width * y + x];
                    dest->data[dest->width * x + dest->width - y - 1] = p;
                }
            }

            return TRANSFORMATION_OK;
        }
    }
}
